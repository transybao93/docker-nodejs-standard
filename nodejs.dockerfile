FROM node:10.14.1
MAINTAINER TRAN SY BAO
#USER root

# Install Node.js and other dependencies
#RUN apt-get update
#RUN apt-get install -y curl
#RUN apt-get install -y sudo
#RUN curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
#RUN apt-get install -y nodejs
RUN echo "Nodejs version" && node -v
RUN echo "NPM version" && npm -v

# add user group
#RUN groupadd -r nodejs && useradd -m -r -g nodejs nodejs
#USER nodejs
#RUN sudo usermod -aG sudo nodejs

# Copy api source from api/ folder then run `npm install`
# Create an api folder within container
RUN mkdir /api
WORKDIR /api

RUN echo "Copying api..."
COPY ./api /api

#Running the app
EXPOSE 8001
#CMD ["npm", "start"]
CMD ["sh", "/api/npm.sh"]
