#!/bin/bash

# remove existed api folder
echo "Removing api folder..."
rm -rf ./api

# Cloning
echo "Cloning..."
git clone https://gitlab.com/transybao93/employee-qr-scan.git ./api

# Copying npm.sh to api folder
echo "Copying npm.sh to api folder..."
cp -v npm.sh ./api
